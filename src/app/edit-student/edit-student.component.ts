import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { student } from '../../constants';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {
  @Input() student: student;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private studentService: StudentService
  ) { }

  ngOnInit() {
    this.getStudentFromRoute();
  }

  getStudentFromRoute(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.warn('id', id);
    this.studentService.getStudentFromId(id).subscribe(
      (eachstudent) => {
        console.warn('eachstudent>>>', eachstudent)
        this.student = eachstudent
      }
    );
  }

  updateStudent(): void {
    this.studentService.updateStudent(this.student).subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back();
  }
}
