import {student} from '../constants';

export const studentList: student[] = [
  {
    id: 1,
    name: 'Nguyen Van B',
    age: 12
  },
  {
    id: 13,
    name: 'Nguyen Van Bsk',
    age: 22
  },
  {
    id: 12,
    name: 'Nguyen Van Bask',
    age: 152
  },
  {
    id: 14,
    name: 'Nguyen Van Bssk',
    age: 5
  },
  {
    id: 15,
    name: 'Nguyen Van Bsgk',
    age: 126
  },
  {
    id: 16,
    name: 'Nguyen Van skB',
    age: 120
  },
  {
    id: 17,
    name: 'Nguyen Van Bhsk',
    age: 122
  },
  {
    id: 18,
    name: 'Nguyen Van Bs5k',
    age: 123
  },
  {
    id: 19,
    name: 'Nguyen Van B3sk',
    age: 124
  },
  {
    id: 10,
    name: 'Nguyen Van Bsk3',
    age: 125
  }
];
