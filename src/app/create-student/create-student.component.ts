import { Component, OnInit } from '@angular/core';
import { student } from '../../constants';
import { StudentService } from '../services/student.service';

import { Location } from '@angular/common'

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {
  newStudent: student[]
  constructor(
    private studentService: StudentService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  add(
    firstName: string,
    lastName: string,
    age: number,
    gender: string,
    size: string)
    :void
    {
      const newStudent: student = new student();
      newStudent.firstName = firstName
      newStudent.lastName = lastName
      newStudent.age = age
      newStudent.gender = gender
      newStudent.size = size
      this.studentService.createStudent(newStudent)
        .subscribe(createdStudent => this.goBack()
      )
    }

  goBack(): void {
    this.location.back();
  }
}
