import { Component, OnInit, Input } from '@angular/core';
import { student } from '../../constants';

@Component({
  selector: 'app-edit-student-modal',
  templateUrl: './edit-student-modal.component.html',
  styleUrls: ['./edit-student-modal.component.css']
})
export class EditStudentModalComponent implements OnInit {
  @Input() student: student;
  constructor() { }
  close() {
    this.student = null;
  }
  ngOnInit() {
  }
}
