import { Injectable } from '@angular/core';
import { student } from '../../constants';
import { studentList } from '../student-list';

// Get data async with Observable
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators'

// Get message services
import { MessageService } from './message.service';

// Call API from JSON server
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}
@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private studentURL = 'http://localhost:3000/students'
  getStudent(): Observable<student[]> {
    const today = new Date().toLocaleDateString();
    // this.messageService.add(today);
    // return of(studentList);
    return this.http.get<student[]>(this.studentURL).pipe(
      tap(res => console.warn('res', res)),
      catchError(e => of(console.warn[e]))
    );
  }
  getStudentFromId(id: number): Observable<student> {
    // return of(studentList.find(eachStudent => eachStudent.id === id));
    const eachStudentUrlId = `${this.studentURL}/${id}`
    console.warn('eachStudentURL', eachStudentUrlId)
    return this.http.get<student>(eachStudentUrlId).pipe(
      tap(resp => console.warn('resp>>', resp)),
      catchError(e => of(new student()))
    )
  }
  updateStudent(student: student): Observable<any> {
    const eachStudentUrl = `${this.studentURL}/${student.id}`
    return this.http.put(eachStudentUrl, student, httpOptions).pipe(
      tap(res => console.warn('Put respone', res)),
      catchError(error => of([console.error(error)
      ]))
    )
  }
  createStudent(newStudent: student): Observable<student> {
    return this.http.post<student>(this.studentURL, newStudent, httpOptions).pipe(
      tap(res => console.warn('Post respone', res)),
      catchError(e => of(new student()))
    )
  }
  deleteStudent(studentId: number): Observable<student> {
    const eachStudentUrlId = `${this.studentURL}/${studentId}`
    return this.http.delete<student>(eachStudentUrlId, httpOptions).pipe(
      tap(_ => console.warn(`Student has been delete = ${studentId}`)),
      catchError(e => of(null))
    )
  }
  constructor(
    private http: HttpClient,
    public messageService: MessageService
  ) { }
}
