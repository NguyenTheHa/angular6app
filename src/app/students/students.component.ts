import { Component, OnInit } from '@angular/core';
import { student } from '../../constants';
// import { studentList } from '../student-list';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  constructor(private studentService: StudentService) {}
  student: student = {
    id: 1,
    firstName: 'Nguyen',
    lastName: 'Van A',
    gender: 'Male',
    size: 'XXL',
    age: 30
  };
  // studentLists = studentList;
  studentLists: student[];

  selectedStudent: student;

  ngOnInit() {
    this.getStudentFromServices();
  }

  onBlurStudent(selectStudent: student): void {
    this.selectedStudent = selectStudent;
  }

  getStudentFromServices(): void {
    // this.studentLists = this.studentService.getStudent();
    this.studentService.getStudent().subscribe(
      (updatedStudent) => {
        this.studentLists = updatedStudent;
        console.warn('updatedStudent', updatedStudent);
      }
    );
  }

  deleteStudent(studentId: number): void {
    this.studentService.deleteStudent(studentId).subscribe( _ => {
      this.studentLists = this.studentLists.filter(deletedStudent => deletedStudent.id !== studentId)
    })
  }
}
