export class student {
    id: Number
    firstName: String
    lastName: String
    gender: String
    age: Number
    size: String
}